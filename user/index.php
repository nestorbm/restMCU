
<?php session_start(); 

  if($_SESSION['name'] == null){
    header('Location: ' . "/", true, $permanent ? 301 : 302);
    exit();
  }else if ($_SESSION['role'] == "ROLE_USER"){
    $_SESSION['page'] = "Home";
  }else{
    echo '<script language="javascript">';
    echo 'alert("Usuario no autorizado");';
    echo 'window.location= "../";';
    echo '</script>';
  }

?><!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <title>Welcome to MCU</title>

    <!-- Add to homescreen for Chrome on Android -->
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="icon" sizes="192x192" href="../images/android-desktop.png">

    <!-- Add to homescreen for Safari on iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Material Design Lite">
    <link rel="apple-touch-icon-precomposed" href="images/ios-desktop.png">

    <!-- Tile icon for Win8 (144x144 + tile color) -->
    <meta name="msapplication-TileImage" content="images/touch/ms-touch-icon-144x144-precomposed.png">
    <meta name="msapplication-TileColor" content="#3372DF">

    <link rel="shortcut icon" href="../images/favicon.png">

    <!-- SEO: If your mobile URL is different from the desktop URL, add a canonical link to the desktop page https://developers.google.com/webmasters/smartphone-sites/feature-phones -->
    <!--
    <link rel="canonical" href="http://www.example.com/">
    -->

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.cyan-light_blue.min.css">
    <link rel="stylesheet" href="../css/styles.css">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>

    <script type="text/javascript" src="../js/dataTables.min.js"></script>
    <link rel="stylesheet" href="../css/dataTables.min.css">

    <script type="text/javascript">
         $(document).ready( function () {
            $('#myTable').DataTable();
            $('#myTable2').DataTable();
         } );
    </script>
    <style>
    #view-source {
      position: fixed;
      display: block;
      right: 0;
      bottom: 0;
      margin-right: 40px;
      margin-bottom: 40px;
      z-index: 900;
    }
    </style>
  </head>
  <body>
    <div class="demo-layout mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
      <?php include('menu.php') ?>

      <main class="mdl-layout__content mdl-color--grey-100">
        <div class="mdl-grid demo-content">

          <div class="demo-updates mdl-card mdl-shadow--2dp mdl-cell mdl-cell--4-col mdl-cell--12-col-tablet mdl-cell--12-col-desktop">
            <div style="background-color: white; height: 4px; width: 80%; margin: 0 auto; margin-top: 8px"></div>
            <h4 style="margin: 0 auto; padding-top: 12px;"><strong>MY BATTLES</strong></h4>
            <div style="background-color: black; height: 4px; width: 80%; margin: 0 auto; margin-top: 8px; margin-bottom:20px;"></div>
            <form action="seeResult.php" method="POST">
            <center>
                <table id="myTable" class="table table-striped">
                  <thead>
                    <tr>
                      <th scope="col">Character 1</th>
                      <th scope="col">Character 2</th>
                      <th scope="col"></th>
                    </tr>
                  </thead class="thead-dark">
                  <tbody>
                    <?php
                    $data = array("userID" => $_SESSION['userID']);
                    $headers  = [
                          'Accept:application/json;',
                          'Content-Type: application/x-www-form-urlencoded',
                          ('Authorization:'.$_SESSION['token'])
                              ];

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL,$_SESSION['backendIP']."/api/fight/myCurrentFights/");
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

                    $result = curl_exec($ch);
                    $data = json_decode($result, true);
                    $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                    for ($i=0;$i<count($data)-1;$i++){
                      if($i == 0){
                        $character1 = $data[$i];
                        $character2 = $data[$i+1];
                      }else{
                        $character1 = $data[$i];
                        $character2 = $data[$i+1];
                      }
                      echo "<tr> <th scope='row'>".$character1[0]."</th>";
                      echo "<td align='center'>".$character2[0]."</td>";
                      echo "<td align='center'> <button type='submit' class='btn btn-primary' value=".$character2[1]." name='battle'>See the votations!</button></td>";
                      echo "</tr>";
                      $i++;
                    }
                    
                    ?>
                </tbody>
              </table>
            </center>
          </form>

          </div>
           <div class="demo-updates mdl-card mdl-shadow--2dp mdl-cell mdl-cell--4-col mdl-cell--12-col-tablet mdl-cell--12-col-desktop" style="background-color: #266791;">
            <div style="background-color: #266791; height: 4px; width: 80%; margin: 0 auto; margin-top: 8px"></div>
            <h4 style="margin: 0 auto; padding-top: 12px; color:white"><strong>SEE THE RESULTS OF YOUR ENDED BATTLES!</strong></h4>
            <div style="background-color: white; height: 4px; width: 80%; margin: 0 auto; margin-top: 8px; margin-bottom:20px;">
              <br>
            </div>
            <form action="seeResult.php" method="POST" style="background-color: white">
            <center style="padding-top: 2%">
                <table id="myTable2" class="table table-striped">
                  <thead>
                    <tr style="color: black">
                      <th scope="col">Character 1</th>
                      <th scope="col">Character 2</th>
                      <th scope="col"></th>
                    </tr>
                  </thead >
                  <tbody>
                    <?php

                    $data = array("userID" => $_SESSION['userID']);
                    $headers  = [
                          'Accept:application/json;',
                          'Content-Type: application/x-www-form-urlencoded',
                          ('Authorization:'.$_SESSION['token'])
                              ];

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL,$_SESSION['backendIP']."/api/fight/myEndedFights/");
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

                    $result = curl_exec($ch);
                    $data = json_decode($result, true);
                    $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                    for ($i=0;$i<count($data)-1;$i++){
                      if($i == 0){
                        $character1 = $data[$i];
                        $character2 = $data[$i+1];
                      }else{
                        $character1 = $data[$i];
                        $character2 = $data[$i+1];
                      }
                      echo "<tr> <th scope='row'>".$character1[0]."</th>";
                      echo "<td align='center'>".$character2[0]."</td>";
                      echo "<td align='center'> <button type='submit' class='btn btn-primary' value=".$character1[1]." name='battle'>See the result!</button></td>";
                      echo "</tr>";
                      $i++;
                    }
                    
                    ?>
                </tbody>
              </table>
            </center>
          </form>
          </div>
          <!-- CIERRE GENERAL -->

        </div>
      </main>
    </div>
    <script src="https://code.getmdl.io/1.3.0/material.min.js"></script>
  </body>
</html>
