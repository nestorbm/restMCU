<?php
session_start();
$ch = curl_init();
$headers  = [
				'Authorization:'.$_SESSION['token'],
				'Accept:application/json;',
		        'Content-Type:application/json'
			];
$postData = ["user" => $_SESSION['userID'], "character" => $_SESSION['character']];
curl_setopt($ch, CURLOPT_URL,$_SESSION['backendIP']."/api/favorites/new");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData));           
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
$result     = curl_exec ($ch);
$statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
curl_close($ch);
if($statusCode == 200){
	echo '<script language="javascript">';
	echo 'alert("Added Successfully");';
	echo 'window.location= "../myFav.php";';
	echo '</script>';
}else{
	echo '<script language="javascript">';
	echo 'alert("Error try again later");';
	echo 'window.location= "../characters.php";';
	echo '</script>';
}