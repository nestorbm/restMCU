<?php
session_start();

// GET THE FIELDS TO MODIFY

$username = $_SESSION['name'];
$pass = $_POST['password'];

// GET THE API TOKEN FOR MODIFY THE USER
$token = $_SESSION['API_TOKEN'];

$id = $_SESSION['userID'];


try{		

		// SENT THE PUT REQUEST TO MODIFY THE USER
		$data = array("id" => $id,"username" => $username,"password"=>$pass);
		$headers  = [
					'Accept:application/json;',
					'Content-Type: application/x-www-form-urlencoded',
					('Authorization:'.$token)
			        ];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$_SESSION['backendIP']."/api/user/modify");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		$response = json_decode($result, true);
		$statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		
		if($statusCode == 200){
			echo '<script language="javascript">';
			echo 'alert("Modificado Exitosamente");';
			echo 'window.location= "../";';
			echo '</script>';
		}else{
			echo '<script language="javascript">';
			echo 'alert("Error intente mas tarde");';
			echo 'window.location= "../";';
			echo '</script>';
			}
			
	}catch(Exception $e){
	echo "Error comuniquese co";
}