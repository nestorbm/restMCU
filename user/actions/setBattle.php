<?php
session_start();

if(isset($_POST['characters'])){
	$leftSide = $_POST['characters'];
	$hero1=$_POST['heroes'];
	$villian1=$_POST['villians'];
}else{
	echo '<script language="javascript">';
	echo 'alert("Chose an option on the left side");';
	echo 'window.location= "../battle.php";';
	echo '</script>';
}

if(isset($_POST['characters2'])){
	$rightSide = $_POST['characters2'];
	$hero2=$_POST['heroes2'];
	$villian2=$_POST['villians2'];
}else{
	echo '<script language="javascript">';
	echo 'alert("Chose an option on the right side");';
	echo 'window.location= "../battle.php";';
	echo '</script>';}

$leftChracter = "";
$rightCharacter = "";


// DETERMINATION OF THE LEFT SIDE CHARACTER

if($leftSide == 'villian'){
	if($villian1 != -1){
		$leftChracter = $villian1;
	}else{
		echo '<script language="javascript">';
		echo 'alert("No villian choosed on the left side");';
		echo 'window.location= "../battle.php";';
		echo '</script>';
	}
}else if($leftSide == 'hero'){
	if($hero1 != -1){
		$leftChracter = $hero1;
	}else{
		echo '<script language="javascript">';
		echo 'alert("No hero choosed on the left side");';
		echo 'window.location= "../battle.php";';
		echo '</script>';
	}
}

// DETERMINATION OF THE RIGHT SIDE CHARACTER

if($rightSide == 'villian2'){
	if($villian2 != -1){
		$rightCharacter = $villian2;
	}else{
		echo '<script language="javascript">';
		echo 'alert("No villian choosed on the right side");';
		echo 'window.location= "../battle.php";';
		echo '</script>';
	}
}else if($rightSide == 'hero2'){
	if($hero2 != -1){
		$rightCharacter = $hero2;
	}else{
		echo '<script language="javascript">';
		echo 'alert("No hero choosed on the right side");';
		echo 'window.location= "../battle.php";';
		echo '</script>';
	}
}

if($leftChracter != $rightCharacter ){
	$deadline = $_POST['deadline'];
	$tz = 'America/Bogota';
	$timestamp = time();
	$dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
	$dt->setTimestamp($timestamp); //adjust the object to correct timestamp
	$today = $dt->format('Y-m-d');

	if($deadline > $today){
		setBattle($deadline,$leftChracter,$rightCharacter);
	}else{
		echo '<script language="javascript">';
		echo 'alert("The deadline cannot be today or before");';
		echo 'window.location= "../battle.php";';
		echo '</script>';
	}
}else{
	echo '<script language="javascript">';
	echo 'alert("Choose diferent characters");';
	echo 'window.location= "../battle.php";';
	echo '</script>';
}

function setBattle($deadline,$leftSide, $rightSide){
		$token = $_SESSION['token'];	
		$ch = curl_init();
		$headers  = [
					'Accept:application/json;',
		            'Content-Type:application/json',
		            ('Authorization:'.$token)
		        ];
		$postData = ["user" => $_SESSION['userID'], "deadline" => $deadline];
		curl_setopt($ch, CURLOPT_URL,$_SESSION['backendIP']."/api/battle/new");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData));           
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$result     = curl_exec ($ch);
		$statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		$response = json_decode($result, true);
		$battleID = $response['id'];

		setFight($battleID,$leftSide, $rightSide);
}

function setFight ($battleID,$leftSide, $rightSide){
		$data = array("battle" => $battleID,"character1" => $leftSide,"character2"=>$rightSide);
		$headers  = [
					'Accept:application/json;',
					'Content-Type: application/x-www-form-urlencoded',
					('Authorization:'.$_SESSION['token'])
			        ];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$_SESSION['backendIP']."/api/fight/new");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		$response = json_decode($result, true);
		$statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		if($statusCode == 200){
			echo '<script language="javascript">';
			echo 'alert("Your Battle is ON!");';
			echo 'window.location= "../onMCU.php";';
			echo '</script>';
		}else{
			echo '<script language="javascript">';
			echo 'alert("Failed, plese contact the administrator");';
			echo 'window.location= "../onMCU.php";';
			echo '</script>';
			echo $response;
			}
}