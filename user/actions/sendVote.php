<?php
session_start();

if( isset($_POST["leftSide"])){
	$vote = $_POST["leftSide"];
	// POSITION 0 HAS THE CHARACTER ID AND POSITION 1 HAS THE BATTLE ID
	$arr = preg_split('/(?<=[0-9])(-)/i',$vote);
	$character = $arr[0];
	$battle = $arr[1];

	$ch = curl_init();
	$headers  = [
				'Accept:application/json;',
	            'Content-Type:application/json',
	            ('Authorization:'.$_SESSION['token'])
	        ];
	$postData = ["user" => $_SESSION['userID'],"battle" => $battle,"votes_character1"=>"1","votes_character2"=>"0"];
	curl_setopt($ch, CURLOPT_URL,$_SESSION['backendIP']."/api/vote/");
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData));           
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	$result     = curl_exec ($ch);
	$statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	$response = json_decode($result, true);
	curl_close($ch);
	$response = json_decode($result, true);
	
	if($statusCode == 200){
		echo '<script language="javascript">';
		echo 'alert("Your vote was sent!");';
		echo 'window.location= "../onMCU.php";';
		echo '</script>';
	}else{
		echo '<script language="javascript">';
		echo 'alert("Try again later");';
		echo 'window.location= "../onMCU.php";';
		echo '</script>';
	}

}else if (isset($_POST["rightSide"])){
	$vote = $_POST["rightSide"];
	// POSITION 0 HAS THE CHARACTER ID AND POSITION 1 HAS THE BATTLE ID
	$arr = preg_split('/(?<=[0-9])(-)/i',$vote);
	$character = $arr[0];
	$battle = $arr[1];

	$ch = curl_init();
	$headers  = [
				'Accept:application/json;',
	            'Content-Type:application/json',
	            ('Authorization:'.$_SESSION['token'])
	        ];
	$postData = ["user" => $_SESSION['userID'],"battle" => $battle,"votes_character1"=>"0","votes_character2"=>"1"];
	curl_setopt($ch, CURLOPT_URL,$_SESSION['backendIP']."/api/vote/");
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData));           
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	$result     = curl_exec ($ch);
	$statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	$response = json_decode($result, true);
	curl_close($ch);
	$response = json_decode($result, true);
	
	if($statusCode == 200){
		echo '<script language="javascript">';
		echo 'alert("Your vote was sent!");';
		echo 'window.location= "../onMCU.php";';
		echo '</script>';
	}else{
		echo '<script language="javascript">';
		echo 'alert("Try again later");';
		echo 'window.location= "../";';
		echo '</script>';
	}
}