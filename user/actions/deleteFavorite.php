<?php
session_start();
$data = array("userID" => $_SESSION['userID'],"characterID" => $_SESSION['character']);
$headers  = [
			'Accept:application/json;',
			'Content-Type: application/x-www-form-urlencoded',
			('Authorization:'.$_SESSION['token'])
	        ];
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL,$_SESSION['backendIP']."/api/favorites/delete");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

$result = curl_exec($ch);
$response = json_decode($result, true);
$statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
curl_close($ch);

if($statusCode == 200){
	echo '<script language="javascript">';
	echo 'alert("Deleted Successfully");';
	echo 'window.location= "../myFav.php";';
	echo '</script>';
}else{
	echo '<script language="javascript">';
	echo 'alert("Error try again later");';
	echo 'window.location= "../characters.php";';
	echo '</script>';
}