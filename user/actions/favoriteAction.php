<?php
session_start();
if(isset($_POST['addFav'])){
	header('Location: ' . "addFavorite.php", true, $permanent ? 301 : 302);
	$_SESSION['character'] = $_POST['addFav'];
    exit();
}else if(isset($_POST['deleteFav'])){
	header('Location: ' . "deleteFavorite.php", true, $permanent ? 301 : 302);
	$_SESSION['character'] = $_POST['deleteFav'];
    exit();
}