<header class="demo-header mdl-layout__header mdl-color--grey-100 mdl-color-text--grey-600">
        <div class="mdl-layout__header-row">
          <span class="mdl-layout-title"><?php echo$_SESSION['page']?></span>
        </div>
      </header>
      <div class="demo-drawer mdl-layout__drawer mdl-color--blue-grey-900 mdl-color-text--blue-grey-50">
        <header class="demo-drawer-header">
          <img src="../images/mcu.png" width="210px">
          <div class="demo-avatar-dropdown">
            <span>Hello <strong> <?php echo $_SESSION['name']; ?> </strong></span>
            <div class="mdl-layout-spacer"></div>
          </div>
        </header>
        <nav class="demo-navigation mdl-navigation mdl-color--blue-grey-800">
          <a class="mdl-navigation__link" href="index.php"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">home</i>Home</a>
          <a class="mdl-navigation__link" href="battle.php"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">flag</i>Battle</a>

          <a class="mdl-navigation__link" href="characters.php"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">perm_identity</i>Characters</a>

          <a class="mdl-navigation__link" href="myFav.php"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">bookmark</i>My Favorites</a>
          
          <a class="mdl-navigation__link" href="onMCU.php"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">blur_on</i>Battles on MCU</a>
          <a class="mdl-navigation__link" href="me.php"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">account_circle</i>My Profile</a>
          
          <div class="mdl-layout-spacer"></div>

          <a class="mdl-navigation__link" href="../logout.php"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">exit_to_app</i>Logout</a>
        </nav>
      </div>