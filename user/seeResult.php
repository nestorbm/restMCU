
<?php session_start(); 

  if($_SESSION['name'] == null){
    header('Location: ' . "/", true, $permanent ? 301 : 302);
    exit();
  }else if ($_SESSION['role'] == "ROLE_USER"){
    $_SESSION['page'] = "Results";
  }else{
    echo '<script language="javascript">';
    echo 'alert("Usuario no autorizado");';
    echo 'window.location= "../";';
    echo '</script>';
  }

?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <title>Welcome to MCU</title>

    <!-- Add to homescreen for Chrome on Android -->
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="icon" sizes="192x192" href="../images/android-desktop.png">

    <!-- Add to homescreen for Safari on iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Material Design Lite">
    <link rel="apple-touch-icon-precomposed" href="images/ios-desktop.png">

    <!-- Tile icon for Win8 (144x144 + tile color) -->
    <meta name="msapplication-TileImage" content="images/touch/ms-touch-icon-144x144-precomposed.png">
    <meta name="msapplication-TileColor" content="#3372DF">

    <link rel="shortcut icon" href="../images/favicon.png">

    <!-- SEO: If your mobile URL is different from the desktop URL, add a canonical link to the desktop page https://developers.google.com/webmasters/smartphone-sites/feature-phones -->
    <!--
    <link rel="canonical" href="http://www.example.com/">
    -->

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.cyan-light_blue.min.css">
    <link rel="stylesheet" href="../css/styles.css">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>

    <script type="text/javascript" src="../js/dataTables.min.js"></script>
    <link rel="stylesheet" href="../css/dataTables.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

    <style>
    #view-source {
      position: fixed;
      display: block;
      right: 0;
      bottom: 0;
      margin-right: 40px;
      margin-bottom: 40px;
      z-index: 900;
    }
    </style>
  </head>
  <body>
    <div class="demo-layout mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
      <?php include('menu.php');

        $fightID = $_POST["battle"];
        $data = array("fightID" => $fightID);
        $headers  = [
                  'Accept:application/json;',
                  'Content-Type: application/x-www-form-urlencoded',
                  ('Authorization:'.$_SESSION['token'])
                   ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$_SESSION['backendIP']."/api/fight/getFight");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        $response = json_decode($result, true);
        $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);


      // VALIDATE IF THE USER ALREADY VOTED FOR THIS BATTLE
      $battleID = $response[0];


      $dataVotes = array("battleID" => $battleID[2]);
      $headers  = [
        'Accept:application/json;',
        'Content-Type: application/x-www-form-urlencoded',
        ('Authorization:'.$_SESSION['token'])
      ];

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL,$_SESSION['backendIP']."/api/vote/getVotes/");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
      curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($dataVotes));
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

      $result = curl_exec($ch);
      $responseVotes = json_decode($result, true);
      $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

      ?>

      <main class="mdl-layout__content mdl-color--grey-100">
        <div class="mdl-grid demo-content">

          <div class="demo-updates mdl-card mdl-shadow--2dp mdl-cell mdl-cell--4-col mdl-cell--12-col-tablet mdl-cell--12-col-desktop">
            <div style="background-color: white; height: 4px; width: 80%; margin: 0 auto; margin-top: 8px"></div>
            <h4 style="margin: 0 auto; padding-top: 12px;"><strong>Results</strong></h4>
            <div style="background-color: black; height: 4px; width: 80%; margin: 0 auto; margin-top: 8px; margin-bottom:20px;"></div>
              <BR>
              <form action="actions/sendVote.php" method="POST">
                <div class="row" style="width: 90%; margin: 0 auto;">
                  <div class="col-sm-5">
                    <?php 

                    $char1 = $response[0];
                    echo "<center><h4><strong>".$char1[0]."</strong><h4></center>";
                    echo "<p>".$char1[1]."</p>";
                    ?>
                </div>

                <div class="col-sm-2">
                  <center>
                  <h4 style="margin: 0 auto; padding-top: 50%;"><strong>VS</strong></h4>
                  <div style="background-color: black; height: 4px; width: 80%; margin: 0 auto; margin-top: 8px; margin-bottom:50%;"></div>
                </center>
                </div>

                <div class="col-sm-5">
                  <?php
                  $char2 = $response[1];
                  echo "<center><h4><strong>".$char2[0]."</strong><h4></center>";
                  echo "<p>".$char2[1]."</p>";
                  ?>
                </div>
                <br>
                <br>
              </div>

              <div class ="row" style="width: 90%; margin: 0 auto">
                <div class ="col-sm-5">
                  <center><h4 style="margin: 0 auto; padding-top: 12px;"><strong>Votes<?php echo " for ".$char1[0]; ?></strong></h4></center>
                  <div style="background-color: black; height: 4px; width: 80%; margin: 0 auto; margin-top: 8px; margin-bottom:20px;"></div>
                  <?php
                  $votes = $responseVotes[0];
                  if($votes[0] > $votes[1]){
                    echo "<center><h4 style='color: green;'>".$votes[0]."</h4></center>";
                  }else if($votes[0] == $votes[1]){
                    echo "<center><h4 style='color: blue;'>".$votes[0]."</h4></center>";
                  }else{
                    echo "<center><h4 style='color: red;'>".$votes[0]."</h4></center>";
                  }
                  ?>
                </div>

                <div class ="col-sm-2">
                </div>

                <div class ="col-sm-5">
                  <center><h4 style="margin: 0 auto; padding-top: 12px;"><strong>Votes<?php echo " for ".$char2[0]; ?></strong></h4></center>
                  <div style="background-color: black; height: 4px; width: 80%; margin: 0 auto; margin-top: 8px; margin-bottom:20px;"></div>
                  <?php
                  if($votes[1] > $votes[0]){
                    echo "<center><h4 style='color: green;'>".$votes[1]."</h4></center>";
                  }else if($votes[0] == $votes[1]){
                    echo "<center><h4 style='color: blue;'>".$votes[1]."</h4></center>";
                  }else{
                    echo "<center><h4 style='color: red;'>".$votes[1]."</h4></center>";
                  }
                  ?>
                </div>
              </div>

            <form>
            <br>
          </div>
          <!-- CIERRE GENERAL -->

        </div>
      </main>
    </div>
    <script src="https://code.getmdl.io/1.3.0/material.min.js"></script>
  </body>
</html>
