
<?php session_start(); 

  if($_SESSION['name'] == null){
    header('Location: ' . "/", true, $permanent ? 301 : 302);
    exit();
  }else if ($_SESSION['role'] == "ROLE_USER"){
    $_SESSION['page'] = "Battle";
  }else{
    echo '<script language="javascript">';
    echo 'alert("Usuario no autorizado");';
    echo 'window.location= "../";';
    echo '</script>';
  }

?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <title>Welcome to MCU</title>

    <!-- Add to homescreen for Chrome on Android -->
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="icon" sizes="192x192" href="../images/android-desktop.png">

    <!-- Add to homescreen for Safari on iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Material Design Lite">
    <link rel="apple-touch-icon-precomposed" href="images/ios-desktop.png">

    <!-- Tile icon for Win8 (144x144 + tile color) -->
    <meta name="msapplication-TileImage" content="images/touch/ms-touch-icon-144x144-precomposed.png">
    <meta name="msapplication-TileColor" content="#3372DF">

    <link rel="shortcut icon" href="../images/favicon.png">

    <!-- SEO: If your mobile URL is different from the desktop URL, add a canonical link to the desktop page https://developers.google.com/webmasters/smartphone-sites/feature-phones -->
    <!--
    <link rel="canonical" href="http://www.example.com/">
    -->

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.cyan-light_blue.min.css">
    <link rel="stylesheet" href="../css/styles.css">


    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

    <script type="text/javascript" src="../js/dataTables.min.js"></script>
    <link rel="stylesheet" href="../css/dataTables.min.css">

    <!-- Bootstrap Date-Picker Plugin -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

    <style>
    #view-source {
      position: fixed;
      display: block;
      right: 0;
      bottom: 0;
      margin-right: 40px;
      margin-bottom: 40px;
      z-index: 900;
    }
    </style>

    <script type="text/javascript">
        window.onload = function() {

        // LEFT SIDE ACTIONS

        var ex1 = document.getElementById('hero');
        var ex2 = document.getElementById('heroes');
        
        var ex3 = document.getElementById('villian');
        var ex4 = document.getElementById('villians');
        
        ex1.onclick = handler;
        ex3.onclick = handler2;


        // RIGHT SIDE ACTIONS

        var ex5 = document.getElementById('hero2');
        var ex6 = document.getElementById('heroes2');
        
        var ex7 = document.getElementById('villian2');
        var ex8 = document.getElementById('villians2');
        
        ex5.onclick = handler3;
        ex7.onclick = handler4;
        
        function handler() {
          ex2.style.display = "inline-block";
          ex4.style.display = "none";
        }

        function handler2() {
          ex2.style.display = "none";
          ex4.style.display = "inline-block";
        }

        function handler3() {
          ex6.style.display = "inline-block";
          ex8.style.display = "none";
        }

        function handler4() {
          ex6.style.display = "none";
          ex8.style.display = "inline-block";
        }
      }
      $(document).ready(function(){
      var date_input=$('input[name="deadline"]'); //our date input has the name "date"
      var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
      var options={
        format: 'yyyy-mm-dd',
        container: container,
        todayHighlight: true,
        autoclose: true,
      };
      date_input.datepicker(options);
    })

    </script>
  </head>
  <body>
    <div class="demo-layout mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
      <?php include('menu.php') ?>

      <main class="mdl-layout__content mdl-color--grey-100">
        <div class="mdl-grid demo-content">
          <div class="demo-updates mdl-card mdl-shadow--2dp mdl-cell mdl-cell--4-col mdl-cell--12-col-tablet mdl-cell--12-col-desktop">
            <h4 style="margin: 0 auto; padding-top: 12px;"><strong>SET A BATTLE!</strong></h4>
            <form action="actions/setBattle.php" method="POST">
              <div style="background-color: white; height: 4px; width: 80%; margin: 0 auto; margin-top: 8px"></div>
              <br>
              <div class="row">
                <div class="col-sm-5">
                  <center>
                    <h4 style="margin: 0 auto; padding-top: 12px;"><strong>Choose a character</strong></h4>
                    <div style="background-color: black; height: 4px; width: 80%; margin: 0 auto; margin-top: 8px; margin-bottom:20px;"></div>
                    <img src="http://<?php echo $_SESSION['backendIP'];?>/assets/hero.svg" class="img-responsive img-circle" style="width: 60%"/>
                    <br>
                    <div>
                      <label class="radio-inline">
                        <input type="radio" name="characters" value="hero" id="hero"> Hero
                      </label>
                      <span style="display:inline-block; width: 10%;"></span>
                      <label class="radio-inline">
                        <input type="radio" name="characters" value="villian" id="villian"> Villian
                      </label>
                    </div>
                    
                    <!-- HEROES  -->
                    <select class="selectpicker" id="heroes" name="heroes" style="display: none">
                      <option value = "-1">Select One</option>
                      <?php

                      $curl_h = curl_init($_SESSION['backendIP'].'/api/heroes');

                      curl_setopt($curl_h, CURLOPT_HTTPHEADER,
                          array(
                              'Authorization: '.$_SESSION['token'],
                              'Accept: application/json'
                          )
                      );

                      # do not output, but store to variable
                      curl_setopt($curl_h, CURLOPT_RETURNTRANSFER, true);

                      $response = curl_exec($curl_h);
                      $data = json_decode($response, true);
                      for ($i=0;$i<count($data);$i++){
                        $hero = $data[$i];
                        echo "<option value=".$hero['id'].">".$hero['name']."</option>";
                      }
                      curl_close($curl_h);
                      ?>
                    </select>

                    <!-- VILLIANS -->
                    <select class="selectpicker" id="villians" name="villians" style="display: none">
                      <option value = "-1">Select One</option>
                      <?php

                      $curl_h = curl_init($_SESSION['backendIP'].'/api/villians');

                      curl_setopt($curl_h, CURLOPT_HTTPHEADER,
                          array(
                              'Authorization: '.$_SESSION['token'],
                              'Accept: application/json'
                          )
                      );

                      # do not output, but store to variable
                      curl_setopt($curl_h, CURLOPT_RETURNTRANSFER, true);

                      $response = curl_exec($curl_h);
                      $data2 = json_decode($response, true);
                      for ($i=0;$i<count($data2);$i++){
                        $villian = $data2[$i];
                        echo "<option value=".$villian['id'].">".$villian['name']."</option>";
                      }
                      curl_close($curl_h2);
                      ?>
                    </select>
                    <br>
                    <br>
                  </center>
                </div>

                <div class="col-sm-2">
                  <center>
                    <h4 style="margin: 0 auto; padding-top: 12px;"><strong>Vs</strong></h4>
                  </center>
                </div>

                <div class="col-sm-5">
                  <center>
                    <h4 style="margin: 0 auto; padding-top: 12px;"><strong>Choosse a character</strong></h4>
                    <div style="background-color: black; height: 4px; width: 80%; margin: 0 auto; margin-top: 8px; margin-bottom:20px;"></div>
                    <img src="http://<?php echo $_SESSION['backendIP'];?>/assets/villian-2.svg" class="img-responsive img-circle" style="width: 60%"/>
                    <br>
                    <div>
                      <label class="radio-inline">
                        <input type="radio" name="characters2" value="hero2" id="hero2"> Hero
                      </label>
                      <span style="display:inline-block; width: 10%;"></span>
                      <label class="radio-inline">
                        <input type="radio" name="characters2" value="villian2" id="villian2"> Villian
                      </label>
                    </div>
                    <select class="selectpicker" id="heroes2" name="heroes2" style="display: none">
                      <option value = "-1">Select One</option>
                      <?php
                      for ($i=0;$i<count($data);$i++){
                        $hero = $data[$i];
                        echo "<option value=".$hero['id'].">".$hero['name']."</option>";
                      }
                      ?>
                    </select>
                    <select class="selectpicker" id="villians2" name="villians2" style="display: none">
                      <option value = "-1">Select One</option>
                      <?php
                      for ($i=0;$i<count($data2);$i++){
                        $villian = $data2[$i];
                        echo "<option value=".$villian['id'].">".$villian['name']."</option>";
                      }
                      ?>
                    </select>
                    <br>
                    <br>
                  </center>
                </div>
              
              </div>

              <div class="form-group"style="width: 40%; margin: 0 auto"> <!-- Date input -->
                <label class="control-label" for="deadline">Deadline</label>
                <input class="form-control" id="deadline" name="deadline" placeholder="YYYY-MM-DD" type="text" required="true" />
                <br>
                <center>
                  <button type="submit" class="btn btn-primary">Start Battle</button>
                </center>
              <br>
              </div>
            </form>
          </div>
         
          <!-- CIERRE GENERAL -->

        </div>
      </main>
    </div>
    <script src="https://code.getmdl.io/1.3.0/material.min.js"></script>
  </body>
</html>
