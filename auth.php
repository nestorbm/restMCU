<?php
	$username = $_POST['email'];
	$pass = $_POST['pass'];
	session_start();
	$_SESSION['backendIP']="35.237.216.116:8080";
	//$_SESSION['backendIP']="localhost:8080";
	if($_SERVER["REQUEST_METHOD"] == "POST") {
		
		$ch = curl_init();
		$headers  = [
					'Accept:application/json;',
		            'Content-Type:application/json'
		        ];
		$postData = ["username" => $username, "password" => $pass];
		curl_setopt($ch, CURLOPT_URL,$_SESSION['backendIP']."/api/login");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData));           
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$result     = curl_exec ($ch);
		$statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$response = json_decode($result, true);
		curl_close($ch);

		$token = "Bearer ".$response['access_token'];
		
		$_SESSION['token'] = $token;

		$roles = $response['roles'];

		$_SESSION['role'] = $roles[0];
		$_SESSION['name'] = $response['username'];

		$_SESSION['page'] = "Home";



		// get the api token

		$ch = curl_init();
		$headers  = [
					'Accept:application/json;',
		            'Content-Type:application/json'
			        ];
		$postData = ["username" => "api", "password" => "MCUapi2018funcional"];
		curl_setopt($ch, CURLOPT_URL,$_SESSION['backendIP']."/api/login");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData));           
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$result     = curl_exec ($ch);
		$statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		$response = json_decode($result, true);
		$token = "Bearer ".$response['access_token'];

		$tokenAPI = $token;

		$_SESSION['API_TOKEN'] = $tokenAPI;

		// GET THE USER ID

		$ch = curl_init();
		$headers  = [
					'Accept:application/json;',
		            'Content-Type:application/json',
		            ('Authorization:'.$tokenAPI)
		        ];
		$postData = ["username" => $username];
		curl_setopt($ch, CURLOPT_URL,$_SESSION['backendIP']."/api/user/getID?username=".$username);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);         
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$result     = curl_exec ($ch);
		$statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		$response = json_decode($result, true);

		$id = $response['id'];

		$_SESSION['userID']=$id;

		header('Location: ' . "user/", true, $permanent ? 301 : 302);
    	exit();
}