<?php

$token = getAPIToken();
$username = $_POST['email'];
$newKey = changePassword($username,$token);
if( $newKey!= null && $newKey != false){
	$mailCode = sendMail($newKey,$username);
	if ($mailCode == 202){
		echo '<script language="javascript">';
		echo 'alert("A new password was sent to your email");';
		echo 'window.location= "../";';
		echo '</script>';
	}else{
		echo '<script language="javascript">';
		echo 'alert("Try again later");';
		echo 'window.location= "../";';
		echo '</script>';
	}
}else{
	echo '<script language="javascript">';
	echo 'alert("Check your credentials");';
	echo 'window.location= "../forgot.php";';
	echo '</script>';
}

function changePassword($user,$token){
	try{		

		// SENT THE PUT REQUEST TO MODIFY THE USER
		$data = array("username" => $user);
		$headers  = [
					'Accept:application/json;',
					'Content-Type: application/x-www-form-urlencoded',
					('Authorization:'.$token)
			        ];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,"35.237.216.116:8080"."/api/user/changePass");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		$response = json_decode($result, true);
		$statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		
		if($statusCode == 200){
			return $response['newPass'];
		}else{
			return false;			
		}
	}catch(Exception $e){
	echo "Error comuniquese co";
	}
}


function getAPIToken(){

	$ch = curl_init();
	$headers  = [
				'Accept:application/json;',
			    'Content-Type:application/json'
				];
	$postData = ["username" => "api", "password" => "MCUapi2018funcional"];
	curl_setopt($ch, CURLOPT_URL,"35.237.216.116:8080"."/api/login");
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData));           
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	$result     = curl_exec ($ch);
	$statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	curl_close($ch);
	$response = json_decode($result, true);
	$token = "Bearer ".$response['access_token'];

	$tokenAPI = $token;

	return $tokenAPI;
}


function sendMail ($password,$correo){

	require 'vendor/autoload.php'; // If you're using Composer (recommended)

	$email = new \SendGrid\Mail\Mail(); 
	$email->setFrom("nestor.bernal96@outlook.com", "The MCU Team");
	$email->setSubject("Password change request");
	$email->addTo($correo, "Dear user");
	$email->addContent(
	    "text/html", "<strong>Dear User</strong>
	    <br>
	    <br>
	    <p>We've received a password change request for your account, so here's your new-temporal passowrd</p>	    
	    <strong>Password: </strong>".$password."<br><br>
	    <p>Please, remember to change your password on My Profile section</p>
	    <br>
	    <br>
	    <p>Have fun!</p>
	    <br>
	    <br>
	    <p>Sincerly,</p>
	    <br>
	    <strong>The MCU Accounts Team!</strong>"
	);
	$sendgrid = new \SendGrid('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');
	try {
	    $response = $sendgrid->send($email);
	    return $response->statusCode();
	} catch (Exception $e) {
	    echo "error";
	}
}